# coding=gbk
import jieba
import pandas as pd
import wordcloud
import matplotlib.pyplot as plt
import numpy as np
from scipy.misc import imread

with open('��ӹ-���Ӣ�۴�txt��У��.txt',encoding='gbk') as f:
  text = f.read()
s2 = open('ͣ�ô�.txt','r',encoding="utf-8").read()

dict = '��ӹ����С˵.txt'
jieba.load_userdict(dict)


tmpdf_peo = open('����.txt', 'r',encoding = 'utf-8').read()
tmpdf_pla = open('����.txt', 'r',encoding = 'utf-8').read()
newlist = [ w for w in jieba.lcut(text) if w not in s2 ]
peo_nam = [ w for w in newlist if  w in tmpdf_peo ]
pla_nam = [ w for w in newlist if len(w)>1 and w in tmpdf_pla ]


myfont =  r'C:\user\demo\demo.ttf'
'''
cloudpeo = wordcloud.WordCloud(font_path = myfont,
    mode = "RGBA", background_color = None
    ).generate(' '.join(peo_nam) )
plt.imshow(cloudpeo)
plt.axis("off")
plt.show()

cloudpeo = wordcloud.WordCloud(font_path = myfont,
    mode = "RGBA", background_color = None
    ).generate(' '.join(pla_nam) )
plt.imshow(cloudpeo)
plt.axis("off")
plt.show()
'''


from wordcloud import get_single_color_func

class GroupedColorFunc(object):

    def __init__(self, color_to_words, default_color):
        self.color_func_to_words = [
            (get_single_color_func(color), set(words))
            for (color, words) in color_to_words.items()]

        self.default_color_func = get_single_color_func(default_color)

    def get_color_func(self, word):
        """Returns a single_color_func associated with the word"""
        try:
            color_func = next(
                color_func for (color_func, words) in self.color_func_to_words
                if word in words)
        except StopIteration:
            color_func = self.default_color_func

        return color_func

    def __call__(self, word, **kwargs):
        return self.get_color_func(word)(word, **kwargs)

cloudobj = wordcloud.WordCloud(font_path = myfont,
    mode = "RGBA", background_color = None
    ).generate(' '.join(pla_nam)+' '.join(peo_nam))

color_to_words = {
    'blue': peo_nam,
    'red': pla_nam
}
default_color = 'gray'
grouped_color_func = GroupedColorFunc(color_to_words, default_color)
cloudobj.recolor(color_func=grouped_color_func)
plt.imshow(cloudobj)
plt.axis("off")
plt.show()


imgobj = imread("��񱳾�.jpg")
image_colors = wordcloud.ImageColorGenerator(np.array(imgobj))
cloudobj.recolor(color_func=image_colors)
plt.imshow(cloudobj)
plt.axis("off")
plt.show()
