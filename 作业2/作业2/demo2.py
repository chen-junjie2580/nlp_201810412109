import jieba
import pandas as pd

raw = pd.read_csv("金庸-射雕英雄传txt精校版.txt",
                      names=['txt'], sep='aaa', encoding="gbk", engine='python')

#章节判断用变量预处理
def predeal_variable(raw):
    def m_head(tmpstr):
        return tmpstr[:1]

    def m_mid(tmpstr):
        return tmpstr.find("回 ")

    raw['head'] = raw.txt.apply(m_head)
    raw['mid'] = raw.txt.apply(m_mid)
    raw['len'] = raw.txt.apply(len)
    raw['chap'] = 0
    raw.head(50)

#章节判断
def judge_chapter(raw):
    chapnum = 0
    for i in range(len(raw)):
        if raw['head'][i] == "第" and raw['mid'][i] > 0 and raw['len'][i] < 30 :
            chapnum += 1
        if chapnum >= 40 and raw['txt'][i] == "附录一：成吉思汗家族" :
            chapnum = 0
        raw.loc[i, 'chap'] = chapnum

    #删除临时变量
    del raw['head']
    del raw['mid']
    del raw['len']
    raw.head(50)

#提取出所需章节
def extract_chapter(raw):
    tmpchap = raw[raw['chap'] == 1 ].copy()
    tmpchap.reset_index(drop=True, inplace=True)
    tmpchap['paraidx'] = tmpchap.index
    tmpchap['paralen'] = tmpchap.txt.apply(len)
    # 获取章回中最长的段落
    paratxt = tmpchap[tmpchap.paralen == tmpchap.paralen.max()].txt
    paratxt.reset_index(drop=True, inplace=True)

    #提取数据框中段落最长的元素
    para_result = paratxt[0]
    print("第一回最长段落为：",para_result)


    print("不使用停用词及第三方词库分词:\n", jieba.lcut(para_result))
    return para_result

def word_cut(sentence):
    #加载自定义词库
    dict = "搜狗武侠词库.txt"
    jieba.load_userdict(dict)  #dict为自定义词典的路径
    word_list = jieba.lcut(sentence)
    #print("|".join(word_list))
    print("不使用停用词，使用第三方词库分词:\n", word_list)

    #加载停用词表
    stop_list = []
    with open("停用词.txt",'r',encoding='utf-8') as f:
        for word in f:
            if len(word)>0:
                stop_list.append(word.strip())
    #去停用词
    para_list = []
    for word in word_list:
        if word not in stop_list:
            para_list.append(word)
    return para_list

if __name__=="__main__":

    predeal_variable(raw)
    judge_chapter(raw)
    sentence = extract_chapter(raw)
    result = word_cut(sentence)
    print("使用停用词及第三方词库分词:\n",result)
    print("ok")