import jieba
import pandas as pd
from gensim import corpora
from gensim.models.ldamodel import LdaModel

def m_head(tmpstr):
    return tmpstr[:1]

def m_mid(tmpstr):
    return tmpstr.find("回 ")

def m_cut(intxt):
    return [w for w in jieba.cut(intxt)
            if w not in stopList and len(w) > 1]

raw = pd.read_csv("金庸-射雕英雄传txt精校版.txt",
                  names = ['txt'], sep ='aaa', encoding ="GBK" ,engine='python')
raw['head'] = raw.txt.apply(m_head)
raw['mid'] = raw.txt.apply(m_mid)
raw['len'] = raw.txt.apply(len)
chap_num = 0
for i in range(len(raw)):
    if raw['head'][i] == "第" and raw['mid'][i] > 0 and raw['len'][i] < 30:
        chap_num += 1
    if chap_num >= 40 and raw['txt'][i] == "附录一：成吉思汗家族":
        chap_num = 0
    raw.loc[i, 'chap'] = chap_num
del raw['head']
del raw['mid']
del raw['len']

rawgrp = raw.groupby('chap')
chapter = rawgrp.agg(sum)
chapter = chapter[chapter.index != 0]

stopList = list(pd.read_csv('停用词.txt', names = ['w'], sep = 'aaa',
                            encoding = 'utf-8', engine='python').w)

def topic_num(topicList):
    topicList1 = []
    target_num = ""
    for i in topicList:
        topicList1.append(i[0])
    for j in topicList1:
        target_num += str(j+1)+" "

    return target_num

def motif(chapter):
    chap_list=[m_cut(w) for w in chapter.txt]
    dictionary=corpora.Dictionary(chap_list)
    corpus=[dictionary.doc2bow(text) for text in chap_list]
    for i in [10,20,30]:
        ldaModel=LdaModel(corpus, id2word=dictionary, num_topics=i, passes=2)
        print("打印{}个主题下的信息".format(i))
        for j in ldaModel.print_topics(num_topics=i,num_words=10):
            print(j)

    for chapter_num in range(1, 40, 2):
        query=chapter.txt[chapter_num]
        query_bow=dictionary.doc2bow(m_cut(query))
        print("第{}章的主题编号：{}".format(chapter_num,topic_num(ldaModel.get_document_topics(query_bow))))

motif(chapter)